"use strict";

//import "resource://gre/modules/reflect.jsm";
//import("resource://gre/modules/reflect.jsm");
//ChromeUtils.import("resource://gre/modules/reflect.jsm");
//ChromeUtils.import("resource:///modules/SitePermissions.jsm");

let prefServ = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch);
let typeSelectorPref = new Map([
	[prefServ.PREF_BOOL, {"get":prefServ.getBoolPref, "set":prefServ.setBoolPref}],
	[prefServ.PREF_INT, {"get":prefServ.getIntPref, "set":prefServ.setIntPref}],
	[prefServ.PREF_STRING, {"get":prefServ.getStringPref, "set":prefServ.setStringPref}],
]);
let typeSelectorValue = new Map([
	["string", prefServ.PREF_STRING],
	["boolean", prefServ.PREF_BOOL],
	["number", prefServ.PREF_INT],
]);
console.log(typeSelectorPref);

Components.utils.import("resource://gre/modules/reflect.jsm");

class ConfigAPI extends ExtensionAPI {
	getAPI() {
		return {
			experiments : {
				config : {
					async prefGet(name) { // you must double default args in signature
						return new Promise((resolve, reject)=>{
							try{
								let t = prefServ.getPrefType(name);
								let tFuncs = typeSelectorPref.get(t);
								if(tFuncs){
									let res = tFuncs["get"](name);
									resolve(res);
								}else{
									resolve(null);
								}
							}
							catch(ex){
								reject({"error":ex.message});
							}
						});
					},
					async prefSet(name, value) { // you must double default args in signature
						return new Promise((resolve, reject)=>{
							try{
								let valueT = typeSelectorValue.get(typeof value);
								let tFuncs = typeSelectorPref.get(valueT);
								//let t = prefServ.getPrefType(name);
								
								if(tFuncs){
									let res = tFuncs["set"](name, value);
									resolve(res);
								}else{
									resolve(null);
								}
							}
							catch(ex){
								reject({"error":ex.message});
							}
						});
					},
				},
			},
		};
	}
};

var identifierOfVariable1;
identifierOfVariable1=ConfigAPI; //let, const, class, export or without var don't work. 