about:config WebExtensions API experiment [![Unlicensed work](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/)
=========================================

This repo contains a pair of extensions and one demo webapp.
* `experiment` directory contains the source of the [experimental WebExtensions API](https://firefox-source-docs.mozilla.org/toolkit/components/extensions/webextensions/index.html) for passing some API for editing about:config prefs into WebExtensions. **Install it first.**
* `extension` directory contains a simple extension checking prefs against ghacksuser.js - a privacy-preserving preset for about:config. If there are differences, they can be easily fixed.

Note: WebExtension Experiments support in Firefox other than Nightly is damn bad.