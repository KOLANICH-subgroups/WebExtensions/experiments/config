

function getGHacks(){
	return fetch(browser.extension.getURL("./ghacksuser.js"))
}

let userjsLineRx = /(user_pref)\(("(?:[^"]|\\")+"), (\d+|true|false|"(?:[^"]|\\")+")\);/g;

function parseGHacks(){
	return getGHacks().then(e=>e.text()).then(t => {
		/*browser.experiments.parse.parse(t).then(res=>console.log(res));*/
		let res=new Map();
		for(let [whole, prefType, prefName, prefValue] of t.matchAll(userjsLineRx)){
			prefName = JSON.parse(prefName);
			prefValue = JSON.parse(prefValue);
			res.set(prefName, prefValue);
		}
		res.delete("_user.js.parrot");
		return res;
	})
}

function findIssues(){
	return parseGHacks().then(ghacks => {
		let issues = new Map();
		let promises = [];
		for(let [k, desired] of ghacks){
			try{
				promises.push(browser.experiments.config.prefGet(k).then(actual => {
					console.log(actual, desired);
					if(actual != desired){
						issues.set(k, {"desired":desired, "actual":actual});
					}
				}));
			}catch(ex){
			}
		}
		
		return Promise.all(promises).then(e=>issues);
	});
}