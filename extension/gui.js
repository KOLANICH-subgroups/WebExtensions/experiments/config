"use strict";

function fixerFunc(name, issue){
	let btn = issue["btn"];
	browser.experiments.config.prefSet(name, issue["desired"]).then(res => {
		issue["row"].classList.add("fixed")
		btn.disabled = true;
		allIssues.delete(name);
		if(!allIssues.size){
			fixAllBtn.disabled = true;
		}
	});
}

let allIssues = null;

function fixAll(){
	for(let [k, issue] of allIssues){
		fixerFunc(k, issue);
	}
}

async function checkGHacks(tB, fixAllBtn){
	tB.innerHTML = "";
	allIssues = await findIssues();
	if(allIssues.size){
		fixAllBtn.disabled = false;
	}else{
		fixAllBtn.disabled = true;
	}
	for(let [k, issue] of allIssues){
		console.log(k, issue);
		let r = document.createElement("TR");
		issue["row"] = r;
		let nameEl = document.createElement("TD");
		let nameLink = document.createElement("A");
		nameLink.textContent = k;
		nameLink.href="about:config?filter="+k
		nameEl.appendChild(nameLink);
		r.appendChild(nameEl);
		
		let valueEl = document.createElement("TD");
		r.appendChild(valueEl);
		valueEl.textContent = issue["actual"];
		
		let desiredEl = document.createElement("TD");
		desiredEl.textContent = issue["desired"];
		r.appendChild(desiredEl);
		
		let fixCell = document.createElement("TD");
		let fixBtn = document.createElement("BUTTON");
		issue["btn"] = fixBtn;
		fixBtn.textContent = "FIX";
		fixBtn.addEventListener("click", fixerFunc.bind(null, k, issue), false);
		fixCell.appendChild(fixBtn);
		r.appendChild(fixCell);
		
		tB.appendChild(r);
	}
}

function doCheck(e){
	console.log("doCheck");
	let reEl = document.getElementById("results");
	let fixAllBtn = document.getElementById("fixall");
	let tB = reEl.getElementsByTagName("TBODY")[0];
	fixAllBtn.addEventListener("click", fixAll, false);
	checkGHacks(tB, fixAllBtn);
}

document.addEventListener("DOMContentLoaded", doCheck, false);